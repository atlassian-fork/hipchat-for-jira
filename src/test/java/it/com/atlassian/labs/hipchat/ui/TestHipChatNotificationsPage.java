package it.com.atlassian.labs.hipchat.ui;


import it.com.atlassian.labs.hipchat.pageobjects.AddHipChatPostFunctionParamsPage;
import it.com.atlassian.labs.hipchat.pageobjects.HipChatNotificationsPage;
import it.com.atlassian.labs.hipchat.pageobjects.HipChatNotificationsRow;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class TestHipChatNotificationsPage extends BaseTest
{
    @Test
    public void testMultipleRowsOnNotificationsTable() throws Exception
    {
        createHipChatPostFunction(WORKFLOW_NAME, "To Do", "Start Progress");
        createHipChatPostFunction(WORKFLOW_NAME, "In Progress", "Stop Progress");

        final HipChatNotificationsPage notificationsPage = jira.goTo(HipChatNotificationsPage.class, PROJECT_KEY);
        final List<HipChatNotificationsRow> rows = notificationsPage.getRows();
        assertEquals(2, rows.size());
        assertEquals(WORKFLOW_NAME, rows.get(0).getWorkflowName());
        assertEquals(WORKFLOW_NAME, rows.get(1).getWorkflowName());
        assertNotNull(getRow(rows, "Start Progress"));
        assertNotNull(getRow(rows, "Stop Progress"));
    }

    @Test
    public void testEditLink() throws Exception
    {
        createHipChatPostFunction(WORKFLOW_NAME, "In Progress", "Stop Progress");

        final HipChatNotificationsPage notificationsPage = jira.goTo(HipChatNotificationsPage.class, PROJECT_KEY);
        final HipChatNotificationsRow row = getRow(notificationsPage.getRows(), "Stop Progress");
        row.edit();
        final AddHipChatPostFunctionParamsPage page = jira.getPageBinder().bind(AddHipChatPostFunctionParamsPage.class, "draft", WORKFLOW_NAME, STATUS_IN_PROGRESS_ID, TRANSITION_STOP_PROGRESS_ID, PLUGIN_MODULE_KEY);
        assertEquals(WORKFLOW_NAME, page.getWorkflowName());
        assertEquals(STATUS_IN_PROGRESS_ID, page.getWorkflowStep());
        assertEquals(TRANSITION_STOP_PROGRESS_ID, page.getWorkflowTransition());
    }

    @Test
    public void testEditLinkForInitialTransition() throws Exception
    {
        createHipChatPostFunction(WORKFLOW_NAME, TRANSITION_CREATE_ID);

        final HipChatNotificationsPage notificationsPage = jira.goTo(HipChatNotificationsPage.class, PROJECT_KEY);
        final HipChatNotificationsRow row = getRow(notificationsPage.getRows(), "Create");
        row.edit();
        final AddHipChatPostFunctionParamsPage page = jira.getPageBinder().bind(AddHipChatPostFunctionParamsPage.class, "draft", WORKFLOW_NAME, "", TRANSITION_CREATE_ID, PLUGIN_MODULE_KEY);
        assertEquals(WORKFLOW_NAME, page.getWorkflowName());
        assertEquals("", page.getWorkflowStep());
        assertEquals(TRANSITION_CREATE_ID, page.getWorkflowTransition());
    }

    @Test
    public void testEditLinkForGlobalTransition() throws Exception
    {
        createHipChatPostFunction(WORKFLOW_NAME, "In Progress", "Done");

        final HipChatNotificationsPage notificationsPage = jira.goTo(HipChatNotificationsPage.class, PROJECT_KEY);
        final HipChatNotificationsRow row = getRow(notificationsPage.getRows(), "Done");
        row.edit();
        final AddHipChatPostFunctionParamsPage page = jira.getPageBinder().bind(AddHipChatPostFunctionParamsPage.class, "draft", WORKFLOW_NAME, STATUS_IN_PROGRESS_ID, TRANSITION_DONE_ID, PLUGIN_MODULE_KEY);
        assertEquals(WORKFLOW_NAME, page.getWorkflowName());
        assertEquals("", page.getWorkflowStep());
        assertEquals(TRANSITION_DONE_ID, page.getWorkflowTransition());
    }

    private HipChatNotificationsRow getRow(List<HipChatNotificationsRow> rows, final String issueActionName)
    {
        for (final HipChatNotificationsRow row : rows)
        {
            if (issueActionName.equals(row.getIssueActionName()))
            {
                return row;
            }
        }
        return null;
    }
}
