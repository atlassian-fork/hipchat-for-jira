package com.atlassian.labs.hipchat.test;


import com.atlassian.fugue.Option;
import com.atlassian.hipchat.plugins.api.client.HipChatClient;
import com.atlassian.hipchat.plugins.api.client.HipChatRoomsClient;
import com.atlassian.hipchat.plugins.api.client.Message;
import com.atlassian.hipchat.plugins.api.config.HipChatConfigurationManager;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.index.IssueIndexManager;
import com.atlassian.jira.issue.search.SearchException;
import com.atlassian.jira.issue.status.MockStatus;
import com.atlassian.jira.mock.issue.MockIssue;
import com.atlassian.labs.hipchat.test.mock.HipChatPostFunctionTestOverrider;
import com.atlassian.labs.hipchat.test.mock.MoreAnswers;
import com.atlassian.labs.jira.dto.NotificationDto;
import com.atlassian.labs.jira.notification.HipChatMessageRenderer;
import com.atlassian.labs.jira.workflow.HipChatPostFunction;
import com.atlassian.labs.jira.workflow.HipChatPostFunctionExecutorService;
import com.atlassian.labs.jira.workflow.HipChatPostFunctionFactory;
import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.sal.api.executor.ThreadLocalDelegateExecutorFactory;
import com.atlassian.sal.api.net.ResponseException;
import com.google.common.collect.ImmutableMap;
import com.google.common.util.concurrent.MoreExecutors;
import com.opensymphony.workflow.WorkflowException;
import com.opensymphony.workflow.loader.ActionDescriptor;
import com.opensymphony.workflow.loader.DescriptorFactory;
import com.opensymphony.workflow.loader.ResultDescriptor;
import com.opensymphony.workflow.loader.StepDescriptor;
import com.opensymphony.workflow.loader.WorkflowDescriptor;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.io.IOException;
import java.util.concurrent.ExecutorService;

import static com.atlassian.fugue.Option.some;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;

public class TestHipChatPostFunction {

    private static final int ENDSTEPID = Integer.MAX_VALUE;
    public static final Integer ACTIONID = 1;

    // Mocks
    private ApplicationProperties applicationProperties;
    private ThreadLocalDelegateExecutorFactory executorFactory;
    private HipChatClient hipChatClient;
    private HipChatConfigurationManager configurationManager;
    private HipChatRoomsClient hipChatRoomsClient;
    private HipChatMessageRenderer messageRenderer;

    @Before
    public void setUp() throws Exception {
        applicationProperties = mock(ApplicationProperties.class);
        executorFactory = mock(ThreadLocalDelegateExecutorFactory.class);
        hipChatClient = mock(HipChatClient.class);
        hipChatRoomsClient = mock(HipChatRoomsClient.class);
        configurationManager = mock(HipChatConfigurationManager.class);
        messageRenderer = mock(HipChatMessageRenderer.class);

        Mockito.when(applicationProperties.getBaseUrl()).thenReturn("/fake/base/url/");
        Mockito.when(executorFactory.createExecutorService(Mockito.<ExecutorService>any())).thenReturn(MoreExecutors.sameThreadExecutor());
        Mockito.when(executorFactory.createRunnable((Runnable) Mockito.notNull())).thenAnswer(MoreAnswers.firstArg());
        Mockito.when(messageRenderer.renderNotification(Mockito.anyString(), Mockito.<NotificationDto>anyObject())).thenReturn("fake rendered template");
        Mockito.when(messageRenderer.renderErrorNotification(Mockito.<NotificationDto>anyObject())).thenReturn("fake rendered error template");
        Mockito.when(configurationManager.getApiToken()).thenReturn(some("authtoken"));
        Mockito.when(hipChatClient.rooms()).thenReturn(hipChatRoomsClient);
    }

    @Test
    public void testExecute() throws SearchException, WorkflowException, IOException, ResponseException {
        Issue originalIssue = mock(Issue.class);
        Mockito.when(originalIssue.getStatusObject()).thenReturn(new MockStatus(null, "fake-status-name"));

        postFunction(true, true).execute(
                ImmutableMap.of("issue", mockIssue("ABC-123"), "descriptor", workflowDescriptor(), "actionId", ACTIONID, "originalissueobject", originalIssue),
                ImmutableMap.of(HipChatPostFunctionFactory.JQL_FILTER_PARAM, "fake-jql", HipChatPostFunctionFactory.ROOMS_TO_NOTIFY_CSV_IDS_PARAM, "1,2,3", HipChatPostFunctionFactory.NOTIFY_CLIENTS_PARAM, "true", "username", "fake-username"),
                null);

        Mockito.verify(hipChatRoomsClient, Mockito.times(3)).message(Mockito.anyString(), eq("JIRA"), eq("fake rendered template"), eq(Option.<Message.Format>none()), eq(Option.<Message.BackgroundColor>none()), eq(some(true)));
    }

    @Test
    public void testExecuteIssueWithoutKey() throws SearchException, WorkflowException, IOException, ResponseException {
        Issue originalIssue = mock(Issue.class);
        Mockito.when(originalIssue.getStatusObject()).thenReturn(new MockStatus(null, "fake-status-name"));

        postFunction(false, true).execute(
                ImmutableMap.of("issue", mockIssue(null), "descriptor", workflowDescriptor(), "actionId", ACTIONID, "originalissueobject", originalIssue),
                ImmutableMap.of(HipChatPostFunctionFactory.JQL_FILTER_PARAM, "fake-jql", HipChatPostFunctionFactory.ROOMS_TO_NOTIFY_CSV_IDS_PARAM, "1,2,3", HipChatPostFunctionFactory.NOTIFY_CLIENTS_PARAM, "false", "username", "fake-username"),
                null);

        // Should not cause an exception anymore, but send error message as notifications with red background
        Mockito.verify(hipChatRoomsClient, Mockito.times(3)).message(Mockito.anyString(), eq("JIRA"), eq("fake rendered error template"), eq(Option.<Message.Format>none()), eq(some(Message.BackgroundColor.RED)), eq(some(false)));
    }

    private WorkflowDescriptor workflowDescriptor() {
        WorkflowDescriptor workflowDescriptor = DescriptorFactory.getFactory().createWorkflowDescriptor();
        StepDescriptor stepDescriptor = DescriptorFactory.getFactory().createStepDescriptor();

        stepDescriptor.setId(ENDSTEPID);
        workflowDescriptor.addStep(stepDescriptor);

        ActionDescriptor actionDescriptor = DescriptorFactory.getFactory().createActionDescriptor();
        actionDescriptor.setId(ACTIONID);
        ResultDescriptor resultDescriptor = DescriptorFactory.getFactory().createResultDescriptor();
        resultDescriptor.setStep(ENDSTEPID);
        actionDescriptor.setUnconditionalResult(resultDescriptor);

        workflowDescriptor.addGlobalAction(actionDescriptor);
        return workflowDescriptor;
    }

    private MockIssue mockIssue(String key) {
        MockIssue issue = new MockIssue();
        issue.setKey(key);
        return issue;
    }

    private HipChatPostFunction postFunction(boolean issuePresent, boolean jqlMatches)
    {
        final HipChatPostFunctionExecutorService executorService = new HipChatPostFunctionExecutorService(executorFactory);
        return new HipChatPostFunctionTestOverrider(applicationProperties, null, hipChatClient, configurationManager, executorService, messageRenderer, issuePresent, jqlMatches, mock(IssueIndexManager.class));
    }
}