package com.atlassian.labs.jira.dto.dummy;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.bc.project.component.ProjectComponent;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.attachment.Attachment;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.renderer.IssueRenderContext;
import com.atlassian.jira.issue.issuetype.IssueType;
import com.atlassian.jira.issue.label.Label;
import com.atlassian.jira.issue.priority.Priority;
import com.atlassian.jira.issue.resolution.Resolution;
import com.atlassian.jira.issue.status.Status;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.version.Version;
import org.ofbiz.core.entity.GenericValue;

import javax.annotation.Nullable;
import java.sql.Timestamp;
import java.util.Collection;
import java.util.Set;

/**
 * An issue with dummy data. Used for rendering a preview of an issue notification.
 */
public class DummyIssue implements Issue
{
    private final String key;
    private final String summary;
    private final User assignee;
    private final IssueType issueType;

    public DummyIssue(final String key, final String summary, final User assignee, final IssueType issueType)
    {
        this.key = key;
        this.summary = summary;
        this.assignee = assignee;
        this.issueType = issueType;
    }

    @Override
    public Long getId()
    {
        return null;
    }

    @Override
    public GenericValue getProject()
    {
        return null;
    }

    @Override
    public Project getProjectObject()
    {
        return null;
    }

    @Override
    public Long getProjectId()
    {
        return null;
    }

    @Override
    public GenericValue getIssueType()
    {
        return null;
    }

    @Override
    public IssueType getIssueTypeObject()
    {
        return issueType;
    }

    @Override
    public String getIssueTypeId()
    {
        return null;
    }

    @Override
    public String getSummary()
    {
        return summary;
    }

    @Override
    public User getAssigneeUser()
    {
        return assignee;
    }

    @Override
    public User getAssignee()
    {
        return assignee;
    }

    @Override
    public String getAssigneeId()
    {
        return null;
    }

    @Override
    public Collection<GenericValue> getComponents()
    {
        return null;
    }

    @Override
    public Collection<ProjectComponent> getComponentObjects()
    {
        return null;
    }

    @Override
    public User getReporterUser()
    {
        return null;
    }

    @Override
    public User getReporter()
    {
        return null;
    }

    @Override
    public String getReporterId()
    {
        return null;
    }

    @Override
    public User getCreator()
    {
        return null;
    }

    @Override
    public String getCreatorId()
    {
        return null;
    }

    @Override
    public String getDescription()
    {
        return null;
    }

    @Override
    public String getEnvironment()
    {
        return null;
    }

    @Override
    public Collection<Version> getAffectedVersions()
    {
        return null;
    }

    @Override
    public Collection<Version> getFixVersions()
    {
        return null;
    }

    @Override
    public Timestamp getDueDate()
    {
        return null;
    }

    @Override
    public GenericValue getSecurityLevel()
    {
        return null;
    }

    @Override
    public Long getSecurityLevelId()
    {
        return null;
    }

    @Nullable
    @Override
    public GenericValue getPriority()
    {
        return null;
    }

    @Nullable
    @Override
    public Priority getPriorityObject()
    {
        return null;
    }

    @Override
    public String getResolutionId()
    {
        return null;
    }

    @Override
    public GenericValue getResolution()
    {
        return null;
    }

    @Override
    public Resolution getResolutionObject()
    {
        return null;
    }

    @Override
    public String getKey()
    {
        return key;
    }

    @Override
    public Long getNumber()
    {
        return null;
    }

    @Override
    public Long getVotes()
    {
        return null;
    }

    @Override
    public Long getWatches()
    {
        return null;
    }

    @Override
    public Timestamp getCreated()
    {
        return null;
    }

    @Override
    public Timestamp getUpdated()
    {
        return null;
    }

    @Override
    public Timestamp getResolutionDate()
    {
        return null;
    }

    @Override
    public Long getWorkflowId()
    {
        return null;
    }

    @Override
    public Object getCustomFieldValue(final CustomField customField)
    {
        return null;
    }

    @Override
    public GenericValue getStatus()
    {
        return null;
    }

    @Override
    public Status getStatusObject()
    {
        return null;
    }

    @Override
    public Long getOriginalEstimate()
    {
        return null;
    }

    @Override
    public Long getEstimate()
    {
        return null;
    }

    @Override
    public Long getTimeSpent()
    {
        return null;
    }

    @Override
    public Object getExternalFieldValue(final String s)
    {
        return null;
    }

    @Override
    public boolean isSubTask()
    {
        return false;
    }

    @Override
    public Long getParentId()
    {
        return null;
    }

    @Override
    public boolean isCreated()
    {
        return false;
    }

    @Override
    public Issue getParentObject()
    {
        return null;
    }

    @Override
    public GenericValue getParent()
    {
        return null;
    }

    @Override
    public Collection<GenericValue> getSubTasks()
    {
        return null;
    }

    @Override
    public Collection<Issue> getSubTaskObjects()
    {
        return null;
    }

    @Override
    public boolean isEditable()
    {
        return false;
    }

    @Override
    public IssueRenderContext getIssueRenderContext()
    {
        return null;
    }

    @Override
    public Collection<Attachment> getAttachments()
    {
        return null;
    }

    @Override
    public Set<Label> getLabels()
    {
        return null;
    }

    @Override
    public String getString(final String s)
    {
        return null;
    }

    @Override
    public Timestamp getTimestamp(final String s)
    {
        return null;
    }

    @Override
    public Long getLong(final String s)
    {
        return null;
    }

    @Override
    public GenericValue getGenericValue()
    {
        return null;
    }

    @Override
    public void store()
    {

    }
}
