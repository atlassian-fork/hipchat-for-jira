package com.atlassian.labs.jira.rest;

import com.atlassian.jira.config.IssueTypeManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.labs.jira.dto.dummy.DummyNotificationDto;
import com.atlassian.labs.jira.notification.HipChatMessageRenderer;
import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.templaterenderer.RenderingException;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;

@Path("message")
@Consumes({MediaType.APPLICATION_JSON})
@Produces({MediaType.APPLICATION_JSON})
public class HipChatMessageResource
{
    private final HipChatMessageRenderer messageRenderer;
    private final JiraAuthenticationContext authenticationContext;
    private final ApplicationProperties applicationProperties;
    private final IssueTypeManager issueTypeManager;

    public HipChatMessageResource(final HipChatMessageRenderer messageRenderer, final JiraAuthenticationContext authenticationContext, final ApplicationProperties applicationProperties, final IssueTypeManager issueTypeManager)
    {
        this.messageRenderer = messageRenderer;
        this.authenticationContext = authenticationContext;
        this.applicationProperties = applicationProperties;
        this.issueTypeManager = issueTypeManager;
    }

    @POST
    @Path("render")
    public Response renderMessage(final MessageBean messageBean)
    {
        final String renderedMessage;
        try
        {
            renderedMessage = messageRenderer.renderNotification(messageBean.message, new DummyNotificationDto(authenticationContext, applicationProperties, issueTypeManager));
        }
        catch (IOException e)
        {
            return Response.serverError().build();
        }
        catch (RenderingException e)
        {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        return Response.ok().entity(new MessageBean(renderedMessage)).build();
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    private static class MessageBean
    {
        @JsonProperty
        String message;

        public MessageBean() {}

        public MessageBean(final String message)
        {
            this.message = message;
        }
    }
}
